<?php

use Illuminate\Support\Facades\Route;


use App\Http\Controllers\LicenceController;
use App\Http\Controllers\UtilisateurController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';









Route::get('/home', function () {
    return view('home');
})->name('home');


//Route::get('/user', [UtilisateurController::class,'getAllUtilisateur'])->name('user.list');


//Route licence

Route::get('/lic', [LicenceController::class,'getAllLicence'])->name('lic.list');
Route::get('/ajout', [LicenceController::class,'addLicence'])->name('ajout.licence');
Route::post('/save-licence', [LicenceController::class,'saveLicence'])->name('save.licence');
Route::get('/modifier-licence/{id}', [LicenceController::class,'EditLicence'])->name('modification.lic');
Route::get('/delete-licence/{id}', [LicenceController::class,'DeleteLicence'])->name('delete.licence');
Route::post('/update-licence', [LicenceController::class,'updateLicence'])->name('update.licence');










// Route Utilisateur
Route::get('/user-list', [UtilisateurController::class,'getAllUtilisateur'])->name('util.list');
Route::get('/ajoutuser', [UtilisateurController::class,'addUtilisateur'])->name('ajout.utilisateur');
Route::post('/ajoutuser', [UtilisateurController::class,'saveUtilisateur']);
Route::get('/modifier-utilisateur/{id}', [UtilisateurController::class,'EditUtilisateur'])->name('modification.util');
Route::get('/delete-utilisateur/{id}', [UtilisateurController::class,'DeleteUtilisateur'])->name('delete.utilisateur');
Route::post('/update-utilisateur', [UtilisateurController::class,'updateUtilisateur'])->name('update.utilisateur');




