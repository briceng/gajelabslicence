<?php

namespace Database\Factories;

use App\Models\Licence;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;

class LicenceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Licence::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    $factory->define(Licence::class, function ) {
        return [
            'cle'=> $faker->sentences(6, true),
            'nombre_domaine'=>$faker->paragraphs(2, true),
            'nom'=> $faker->paragraphs(2, true),
            'prenom'=> $faker->paragraphs(2, true),
            'email'=> $faker->paragraphs(2, true),
            'nom_site'=> $faker->paragraphs(2, true),
            'nom_compagnie'=> $faker->paragraphs(2, true),
        ];
    });
    public function definition()
    {
        return [
            //
        ];
    }
}
