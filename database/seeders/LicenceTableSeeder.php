<?php

namespace Database\Seeders;
use App\Licence

use Illuminate\Database\Seeder,

class LicenceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Licence::class, 5)->create();   
    }
}
