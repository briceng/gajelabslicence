<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class UtilisateurController extends Controller
{
    public function getAllUtilisateur() {

        $users = DB::table('users')->get();
        
        return view('liste-utilisateur',compact('users'));

    }


    public function addUtilisateur() {
        return view ('ajouter-utilisateur');
    }


    public function saveUtilisateur(Request $request) {

        $user = User::create([
            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);


    return redirect()->route('util.list');
    }


    public function editUtilisateur($id){

        $licence = DB::table('users')->where('id', $id)->first();
        return view('modifier-utilisateur',compact('utilisateur'));

    }

    public function updateUtilisateur(Request $request)
    {
        DB::table('utilisateurs')->where('id', $request->id)->update([
            'nom'=>$request->nom,
            'prenom'=>$request->prenom,
            'email'=>$request->email,
            'password'=>$request->mot_de_passe,
            'password'=>$request->confirm_password,
        ]);
        return redirect()->route('util.list');
    }


    public function deleteUtilisateur($id)
    {
        DB::table('utilisateurs')->where('id',$id)->delete();
        return redirect()->route('util.list');
    }

}
