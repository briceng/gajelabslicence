<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LicenceController extends Controller
{
    public function getAllLicence() {

        $licences = DB::table('licences')->get();
        
        return view('licence.liste-licence',compact('licences'));

    }

    public function addLicence() {
        return view ('licence.ajouter-licence');
    }

    public function saveLicence(Request $request) {

            DB::table('licences')->insert([
                'cle'=>$request->cle,
                'nombre_domaine'=>$request->nombrededomaine,
                'nom'=>$request->nom,
                'prenom'=>$request->prenom,
                'email'=>$request->email,
                'nom_site'=>$request->nomsite,
                'nom_compagnie'=>$request->nomcompagnie,

            ]);


        return redirect()->route('lic.list');
    }


    public function editLicence($id){

        $licence = DB::table('licences')->where('id', $id)->first();
        return view('licence.modifier-licence',compact('licence'));

    }

    public function updateLicence(Request $request)
    {
        DB::table('licences')->where('id', $request->id)->update([
            'cle'=>$request->cle,
            'nombre_domaine'=>$request->nombrededomaine,
            'nom'=>$request->nom,
            'prenom'=>$request->prenom,
            'email'=>$request->email,
            'nom_site'=>$request->nomsite,
            'nom_compagnie'=>$request->nomcompagnie,
        ]);
        return redirect()->route('lic.list');
    }

    public function deleteLicence($id)
    {
        DB::table('licences')->where('id',$id)->delete();
        return redirect()->route('lic.list');
    }

}
