@extends('layout.app')

@section('title')
<title>Modifier licence</title>
@endsection

@section('content')

<!-- Nested Row within Card Body -->
<div class="row">
                    
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"></h1>
                                   
                            </div>
                            <form class="user" method ="Post" action ="{{route('update.licence')}}"> 
                                @csrf
                                <input type="hidden" class="form-control form-control-user" name="id" value="{{$licence->id}}" >


                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" name="cle" value="{{$licence->cle }}" id="exampleClé"
                                            placeholder="Clé">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control form-control-user" name="nombrededomaine" value="{{$licence->nombre_domaine }}" id="exampleDomaine"
                                            placeholder="Nombre de domaine">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" name="nom" value="{{$licence->nom }}" id="exampleName"
                                            placeholder="Nom">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" name="prenom" value="{{$licence->prenom }}" id="exampleName"
                                            placeholder="Prénom">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user" name="email" value="{{$licence->email }}" id="exampleInputEmail"
                                        placeholder="Adresse Mail">
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user"name="nomsite" value="{{$licence->nom_site }}" id="exampleInputName"
                                        placeholder="Nom du site">
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" name="nomcompagnie" value="{{$licence->nom_compagnie }}"
                                            id="exampleInputName" placeholder="Nom compagnie">
                                    </div>
                                   
                                </div>
                             
                                <button class="btn btn-primary btn-user btn-block" type="submit" > Modifier </button>

                                <hr>
                                
                                
                            </form>
                            
                        </div>
                    </div>
                </div>


@endsection