@extends('layout.app')

@section('title')
<title>Liste licence</title>
@endsection

@section('content')

 <!-- DataTales Example -->
 <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Liste licence</h6>
                        </div>

                        <div class="">
                            <a href="{{route('ajout.licence')}}" class="btn btn-primary float-right ">Ajouter licence</a>
                          </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Clé</th>
                                            <th>Nombre domaine</th>
                                            <th>Nom</th>
                                            <th>Prénom</th>
                                            <th>Adresse Mail</th>
                                            <th>Site</th>
                                            <th>Nom compagnie</th>
                                            <th>Actions</th>
                                           
                                        </tr>
                                    </thead>
                                    
                                    @foreach($licences as $licence)
                                        <tr >
                                            <td>{{$licence->cle}}</td>
                                            <td>{{$licence->nombre_domaine}}</td>
                                            <td>{{$licence->nom}}</td>
                                            <td>{{$licence->prenom}}</td>
                                            <td>{{$licence->email}}</td>
                                            <td>{{$licence->nom_site}}</td>
                                            <td>{{$licence->nom_compagnie}}</td>

                                            <td class="text-center">
                                                <div class="btn-group">
                                                  <a href="/modifier-licence/{{$licence->id}}"  class="btn btn-xs btn-warning" data-toggle="tooltip" title="Edit">
                                                    <span class="glyphicon glyphicon-edit"></span>
                                                  </a>
                                                  <a href="/delete-licence/{{$licence->id}}" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Remove">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                  </a>
                                                </div>
                                              </td>
                                        </tr>
                                      @endforeach  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>



      <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Vous le vous suppprimé???? </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Supprimer</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>


@endsection