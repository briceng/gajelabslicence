@extends('layout.app')

@section('title')
<title>Ajouter licence</title>
@endsection

@section('content')

<!-- Nested Row within Card Body -->
<div class="row">
                    
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"></h1>
                                   
                            </div>
                            <form class="user" method ="Post" action ="{{route('save.licence')}}"> 
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" name="cle" id="exampleClé"
                                            placeholder="Clé">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control form-control-user" name="nombrededomaine" id="exampleDomaine"
                                            placeholder="Nombre de domaine">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" name="nom" id="exampleName"
                                            placeholder="Nom">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" name="prenom" id="exampleName"
                                            placeholder="Prénom">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user" name="email" id="exampleInputEmail"
                                        placeholder="Adresse Mail">
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user"name="nomsite" id="exampleInputName"
                                        placeholder="Nom du site">
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" name="nomcompagnie"
                                            id="exampleInputName" placeholder="Nom compagnie">
                                    </div>
                                   
                                </div>
                             
                                <button class="btn btn-primary btn-user btn-block" type="submit" > Ajouter </button>

                                <hr>
                                
                                
                            </form>
                            
                        </div>
                    </div>
                </div>


@endsection