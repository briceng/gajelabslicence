@extends('layout.app')

@section('title')
<title>Licence encours d'utilisation</title>
@endsection

@section('content')

 <!-- DataTales Example -->
 <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Licences encours d'utilisation</h6>
                        </div>

                        
                        

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Clé</th>
                                            <th>durée</th>
                                            <th>nombre</th>
                                            <th>date creation</th>
                                            <th>date expiration</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    
                                        <tr>
                                            <td>Donna Snider</td>
                                            <td>Customer Support</td>
                                            <td>New York</td>
                                            <td>27</td>
                                            <td>2011/01/25</td>
                                            <td>$112,000</td>

                                            <td class="text-center">
                                                <div class="btn-group">
                                                  <a href=""  class="btn btn-xs btn-warning" data-toggle="tooltip" title="Edit">
                                                    <span class="glyphicon glyphicon-edit"></span>
                                                  </a>
                                                  <a href="" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Remove">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                  </a>
                                                </div>
                                              </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>





@endsection