@extends('layout.app')

@section('title')
<title>Modifier utilisateur</title>
@endsection

@section('content')

<!-- Nested Row within Card Body -->
<div class="row">
                    
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"></h1>
                                   
                            </div>
                            <form class="user" method ="Post" action ="{{route('update.utilisateur')}}">

                            @csrf
                                <input type="hidden" class="form-control form-control-user" name="id" value="{{$user->id}}" >

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" name="nom" value="{{$user->nom }}" id="exampleFirstName"
                                            placeholder="Nom">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" name="prenom" value="{{$user->prenom }}" id="exampleLastName"
                                            placeholder="Prénom">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user"  name="email" value="{{$user->email }}" id="exampleInputEmail"
                                        placeholder="Adresse Mail">
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" class="form-control form-control-user" name="password" value="{{$user->mot_passe }}"
                                            id="exampleInputPassword" placeholder="Mot de passe">
                                    </div>
                                    
                                </div>
                               <button class="btn btn-primary btn-user btn-block" type="submit" > Modifier </button>
                                
                                
                            </form>
                            
                        </div>
                    </div>
                </div>


@endsection