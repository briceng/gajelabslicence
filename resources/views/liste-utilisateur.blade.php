@extends('layout.app')

@section('title')
<title>Liste utilisateur</title>
@endsection

@section('content')

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Liste utilisateur</h6>
                        </div>
                        
                        <div class="">
                            <a href="{{route('ajout.utilisateur')}}" class="btn btn-primary float-right">Ajouter utilisateur</a>
                          </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Prénom</th>
                                            <th>Email</th>
                                            
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{$user->nom}}</td>
                                            <td>{{$user->prenom}}</td>
                                            <td>{{$user->email}}</td>
                                            
                                            
                                            
                                            <td class="text-center">
                                                <div class="btn-group">
                                                  <a href="/modifier-utilisateur/{{$user->id}}"  class="btn btn-xs btn-warning" data-toggle="tooltip" title="Edit">
                                                    <span class="glyphicon glyphicon-edit"></span>
                                                  </a>
                                                  <a href="/delete-utilisateur/{{$user->id}}" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Remove">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                  </a>
                                                </div>
                                              </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


@endsection